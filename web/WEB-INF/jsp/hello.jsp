<%-- 
    Document   : hello
    Created on : Apr 17, 2017, 10:40:07 AM
    Author     : bumerang
    https://www.tutorialspoint.com/spring/spring_mvc_hello_world_example.htm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
      <title>Hello World</title>
   </head>
   
   <body>
      <h2>${message}</h2>
   </body>
</html>