<%-- 
    Document   : ExceptionPage
    Created on : Apr 21, 2017, 10:18:29 AM
    Author     : bumerang
--%>
<!-- https://www.tutorialspoint.com/spring/spring_exception_handling_example.htm -->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
   <head>
      <title>Spring MVC Exception Handling</title>
   </head>

   <body>
      <h2>Spring MVC Exception Handling</h2>
      <h3>${exception.exceptionMsg}</h3>
   </body>
</html>
