/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.staticc.pages.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/*
 * @author    : bumerang    
 * Document   : WebController
 * Created on : Apr 21, 2017, 9:28:19 AM
 */
// https://www.tutorialspoint.com/spring/spring_static_pages_example.htm
@Controller
public class WebController {
   @RequestMapping(value = "/index", method = RequestMethod.GET)
   public String index() {
      return "index";
   }
   @RequestMapping(value = "/staticPage", method = RequestMethod.GET)
   public String redirect() {
      return "redirect:/pages/final.html";
   }
}
