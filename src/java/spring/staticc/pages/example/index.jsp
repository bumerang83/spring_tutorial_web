<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!-- https://www.tutorialspoint.com/spring/spring_static_pages_example.htm -->
<html>
   <head>
      <title>Spring Landing Page Static</title>
   </head>

   <body>
      <h2>Spring Landing Page Static</h2>
      <p>Click below button to get a simple HTML page</p>
      
      <form:form method = "GET" action = "/Spring_Tutorial_Web/staticPage">
         <table>
            <tr>
               <td>
                  <input type = "submit" value = "Get HTML Page"/>
               </td>
            </tr>
         </table>  
      </form:form>
   </body>
   
</html>