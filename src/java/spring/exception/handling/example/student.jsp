<%-- 
    Document   : student
    Created on : Apr 19, 2017, 10:23:08 AM
    Author     : bumerang
    https://www.tutorialspoint.com/spring/spring_mvc_form_handling_example.htm
--%>
<!-- https://www.tutorialspoint.com/spring/spring_exception_handling_example.htm -->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
   <head>
      <title>Spring MVC Exception Handling</title>
   </head>

   <body>
      <h2>Student Information</h2>
      <form:form method = "POST" action = "/Spring_Tutorial_Web/addStudent">
         <table>
            <tr>
               <td><form:label path = "name">Name</form:label></td>
               <td><form:input path = "name" /></td>
            </tr>
            <tr>
               <td><form:label path = "age">Age</form:label></td>
               <td><form:input path = "age" /></td>
            </tr>
            <tr>
               <td><form:label path = "id">id</form:label></td>
               <td><form:input path = "id" /></td>
            </tr>
            <tr>
               <td colspan = "2"><input type = "submit" value = "Submit"/></td>
            </tr>
         </table>  
      </form:form>
   </body>
   
</html>
