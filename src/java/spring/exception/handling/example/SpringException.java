/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.exception.handling.example;

/*
 * @author    : bumerang    
 * Document   : SpringException
 * Created on : Apr 21, 2017, 10:07:11 AM
 */
// https://www.tutorialspoint.com/spring/spring_exception_handling_example.htm
public class SpringException extends RuntimeException {
   private String exceptionMsg;
   
   public SpringException(String exceptionMsg) {
      this.exceptionMsg = exceptionMsg;
   }
   public String getExceptionMsg(){
      return this.exceptionMsg;
   }
   public void setExceptionMsg(String exceptionMsg) {
      this.exceptionMsg = exceptionMsg;
   }
}
