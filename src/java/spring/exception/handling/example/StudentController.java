/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.exception.handling.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;

/*
 * @author    : bumerang    
 * Document   : StudentController
 * Created on : Apr 21, 2017, 10:08:48 AM
 */
// https://www.tutorialspoint.com/spring/spring_exception_handling_example.htm
@Controller
public class StudentController {
   @RequestMapping(value = "/student", method = RequestMethod.GET)
   public ModelAndView student() {
      return new ModelAndView("student", "command", new Student());
   }
   @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
   @ExceptionHandler({SpringException.class})
   public String addStudent( @ModelAttribute("HelloWeb")Student student, 
      ModelMap model) {
      
      if(student.getName().length() < 5 ){
         throw new SpringException("Given name is too short");
      } else {
         model.addAttribute("name", student.getName());
      }
      
      if( student.getAge() < 10 ){
         throw new SpringException("Given age is too low");
      } else {
         model.addAttribute("age", student.getAge());
      }
      model.addAttribute("id", student.getId());
      return "result";
   }
}
