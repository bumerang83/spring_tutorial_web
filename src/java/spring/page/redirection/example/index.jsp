<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!-- https://www.tutorialspoint.com/spring/spring_page_redirection_example.htm -->
<html>
   <head>
      <title>Spring Page Redirection</title>
   </head>

   <body>
      <h2>Spring Page Redirection</h2>
      <p>Click below button to redirect the result to new page</p>
      
      <form:form method = "GET" action = "/Spring_Tutorial_Web/redirect">
         <table>
            <tr>
               <td>
                  <input type = "submit" value = "Redirect Page"/>
               </td>
            </tr>
         </table>  
      </form:form>
   </body>
   
</html>