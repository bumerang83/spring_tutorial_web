<%-- 
    Document   : result
    Created on : Apr 19, 2017, 10:27:25 AM
    Author     : bumerang
    https://www.tutorialspoint.com/spring/spring_mvc_form_handling_example.htm
--%>
<!-- https://www.tutorialspoint.com/spring/spring_mvc_form_handling_example.htm -->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
      <h2>Submitted Student Information</h2>
      <table>
         <tr>
            <td>Name</td>
            <td>${name}</td>
         </tr>
         <tr>
            <td>Age</td>
            <td>${age}</td>
         </tr>
         <tr>
            <td>ID</td>
            <td>${id}</td>
         </tr>
      </table>  
   </body>
   
</html>
