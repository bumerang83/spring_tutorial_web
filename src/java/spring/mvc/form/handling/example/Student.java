/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.mvc.form.handling.example;

/*
 * @author    : bumerang    
 * Document   : Student
 * Created on : Apr 19, 2017, 10:15:30 AM
 */
// https://www.tutorialspoint.com/spring/spring_mvc_form_handling_example.htm
public class Student {
   private Integer age;
   private String name;
   private Integer id;

   public void setAge(Integer age) {
      this.age = age;
   }
   public Integer getAge() {
      return age;
   }
   public void setName(String name) {
      this.name = name;
   }
   public String getName() {
      return name;
   }
   public void setId(Integer id) {
      this.id = id;
   }
   public Integer getId() {
      return id;
   }
}
